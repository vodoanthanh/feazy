(function ($) {
    $.fn.ColorPicker = function () {
        if (this.prop("tagName") == 'SELECT') {
            this.append('<option value="#ffffff" selected>White</option>');
            this.append('<option value="#428ff4">Blue</option>');
            this.append('<option value="#fc0000">Red</option>');
            this.append('<option value="#ffff00">Yellow</option>');
            this.append('<option value="#00ff00">Green</option>');
            this.append('<option value="#ff0066">Pink</option>');
            this.append('<option value="#000000">Back</option>');
            $(this).change(function () {
                $("body").css("background-color", $(this).val());
            });
        }

        return this;
    };
}(jQuery));