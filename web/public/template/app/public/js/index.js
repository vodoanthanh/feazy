(function (HomePage, $, undefined) {
    HomePage.init = function () {
        console.log('This is home page.');
        $(window).scroll(HomePage.scroll);
        $('h1').click(HomePage.click);
    };
    HomePage.scroll = function () {
        console.log(($(window).scrollTop()));
    };

    HomePage.click = function () {
        console.log(($(this).html()));
    };

}(window.HomePage = window.HomePage || {}, jQuery));