module.exports = function (grunt) {
    // Load the Grunt plugins.
    require('matchdep')
        .filterDev('grunt-*')
        .forEach(grunt.loadNpmTasks);

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
                mangle: true,
                compress: {
                }
            },
            'feazy-js-min': {
                files: {
                    'public/build/js/<%= pkg.name %>.min.js': [
                        'public/js/*.js'
                    ]
                }
            }
        }
    });

    // Default task(s).
    grunt.registerTask('default', ['uglify']);

};
