<?php
define('ROOT_PATH', dirname(__DIR__));

$loader = require ROOT_PATH . '/vendor/autoload.php';
$loader->addPsr4('', ROOT_PATH . '/src');

//Config
$config = new \Feazy\Common\Configuration(ROOT_PATH . '/feazy.ini');
\Feazy\Common\DIManager::add('config', $config);

$request = new \Feazy\Common\Request(isset($_GET['route']) ? $_GET['route'] : '');
\Feazy\Common\DIManager::add('request', $request);

\Feazy\Common\DIManager::add('view', new \Feazy\Common\View($config->get('template')));

$router = new \Feazy\Common\Router($request->getParameter('route', '/'));
$router->get('/', 'IndexController@index');
$router->group('/users', function (\Feazy\Common\Router $router) {
    $router->get('/', 'UsersController@index');

    $router->get('/getall', 'UsersController@getAll');

    $router->get('/(\d+)', 'UsersController@getDetail');

    $router->post('/edit', 'UsersController@edit');

    $router->get('/create', 'UsersController@create');

    $router->delete('/deletetest', 'UsersController@deleteTest');
});


echo (new \Application\Controller\IndexController())->errorPage();