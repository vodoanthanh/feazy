<?php
/**
 * Created by PhpStorm.
 * User: thanhvd
 * Date: 2018-10-15
 * Time: 4:40 PM
 */


if (!function_exists('testHelper')) {

    /**
     * @desc This is test function.
     */
    function testHelper()
    {
        echo 'Test Helper';
    }
}

if (!function_exists('curlSendRequest')) {

    /**
     * @param $method
     * @param $url
     * @param array $data
     * @param array $headers
     * @return mixed
     * @desc function use for calling API from backend
     */
    function curlSendRequest($method, $url, $data = array(), $headers = array('Content-Type: application/json'))
    {
        $curl = curl_init();

        /**
         * @var \Feazy\Common\Configuration
         */
        $config = \Feazy\Common\DIManager::get('config');
        $extendHeaders = preg_split('/;/', $config->get('extend_headers'), NULL, PREG_SPLIT_NO_EMPTY);
        $headers = array_merge($headers, $extendHeaders);

        $extendData = preg_split('/;/', $config->get('extend_data'), NULL, PREG_SPLIT_NO_EMPTY);
        foreach ($extendData as $element) {
            list($key, $val) = explode(':', $element);
            $data[$key] = $val;
        }

        $dataJson = json_encode($data);
        switch ($method) {
            case Constants::POST:
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $dataJson);
                break;
            case Constants::PUT:
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, Constants::PUT);
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $dataJson);
                break;
            case Constants::DELETE:
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, Constants::DELETE);
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $dataJson);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        // OPTIONS:
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        // EXECUTE:
        $result = curl_exec($curl);

        if (!$result) {
            die("Connection Failure");
        }
        curl_close($curl);

        return $result;
    }
}