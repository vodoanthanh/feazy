<?php
/**
 * Created by PhpStorm.
 * User: dinhthibc
 * Date: 10/5/18
 * Time: 4:35 PM
 */

namespace Application\Controller;


use Application\Model\Users;
use Firebase\JWT\JWT;


class UsersController extends BaseController
{

    public function index()
    {
        $key = "example_key";
        $token = array(
            "iss" => "http://example.org",
            "aud" => "http://example.com",
            "iat" => 1356999524,
            "nbf" => 1357000000,
        );

        /**
         * IMPORTANT:
         * You must specify supported algorithms for your application. See
         * https://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-40
         * for a list of spec-compliant algorithms.
         */
        $jwt = JWT::encode($token, $key);
        var_dump($jwt);

        try {
            $decoded = JWT::decode($jwt, $key, array('HS256'));
            var_dump($decoded);
        } catch (\Exception $e) {
            print_r($e->getMessage());
            die;
        }

        /*
         NOTE: This will now be an object instead of an associative array. To get
         an associative array, you will need to cast it as such:
        */

        $decoded_array = (array)$decoded;

        /**
         * You can add a leeway to account for when there is a clock skew times between
         * the signing and verifying servers. It is recommended that this leeway should
         * not be bigger than a few minutes.
         *
         * Source: http://self-issued.info/docs/draft-ietf-oauth-json-web-token.html#nbfDef
         */
        JWT::$leeway = 60; // $leeway in seconds
        $decoded = JWT::decode($jwt, $key, array('HS256'));

        var_dump($decoded);


        $this->view->addStyle('node_modules/datatables/media/css/jquery.dataTables.min.css');
        $this->view->addScript('node_modules/datatables/media/js/jquery.dataTables.min.js');
        $this->view->setTitle('Users');
        $this->view->render('users/index');
    }

    public function getAll()
    {
        $limit = $this->request->getParameter('limit', 10);
        $start = $this->request->getParameter('offset', 0);
        $draw = $this->request->getParameter('draw', 1);


        $usersMD = new Users();
        $dataUsers = $usersMD->all('id', 'asc', $start, $limit, '');

        $responseData = array();
        $responseData['totalAll'] = $dataUsers[1];
        $responseData['data'] = $dataUsers[0];

        $this->toJSON($responseData);
    }


    public function getDetail($id)
    {
        $usersMD = new Users();
        $USER_INFO = $usersMD->get($id);

        $this->view->setTitle('DETAIL USER');
        $this->view->render('users/edit', array('USER_INFO' => $USER_INFO));
    }

    public function create()
    {
        $this->view->setTitle('CREATE USER');
        $this->view->render('users/edit', array('USER_INFO' => []));
    }

    public function edit()
    {
        $data = $this->request->getPosts() ?: json_decode(file_get_contents('php://input'), true);

        $usersMD = new Users();
        if (isset($data['insert'])) {
            $id = $usersMD->insert($data);
            if (isset($data['curl'])) {
                return $this->success();
            }
            header('Location: /users/' . $id);
            die;
        }

        if (isset($data['delete'])) {
            $usersMD->delete($data['id']);
            if (isset($data['curl'])) {
                return $this->success();
            }
            header('Location: /users');
            die;
        }

        if (isset($data['update'])) {
            $id = $usersMD->update($data);
            if (isset($data['curl'])) {
                return $this->success();
            }
            header('Location: /users/' . $id);
            die;
        }
    }

    public function deleteTest()
    {
        return $this->success();
    }
}