<?php
/**
 * Created by PhpStorm.
 * User: dinhthibc
 * Date: 10/5/18
 * Time: 4:35 PM
 */

namespace Application\Controller;


use App\User;

class IndexController extends BaseController
{
    public function index()
    {
//        return var_dump(json_decode(curlSendRequest(\Constants::GET, 'http://skeleton.localhost.com/users/getall', array('length' => 1))));
//        return var_dump(json_decode(curlSendRequest(\Constants::POST, 'http://skeleton.localhost.com/users/edit', array(
//            'id' => "16",
//            'first_name' => "Winni",
//            'last_name' => "Sorey",
//            'email' => "wsorey9@craigslist.org",
//            'gender' => "Female",
//            'ip_address' => "234.96.81.157",
//            'delete' => "true",
//            'curl' => "true"
//        ))));


        date_default_timezone_set('Asia/Bangkok');
        $this->view->setTitle('Homepage');
        $this->view->render('index/index', [
            'date' => date('Y/m/d H:i:s')
        ]);
    }

    public function errorPage()
    {
        $this->view->setTitle('ERROR 404');
        $this->view->render('error');
    }
}