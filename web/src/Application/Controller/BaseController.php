<?php
/**
 * Created by PhpStorm.
 * User: dinhthibc
 * Date: 10/5/18
 * Time: 4:36 PM
 */

namespace Application\Controller;


use Feazy\Common\Configuration;
use Feazy\Common\Request;
use Feazy\Controller\Controller;
use Feazy\Common\View;

/***
 * Class BaseController
 * @package Application\Controller
 * @property View view
 */
class BaseController extends Controller
{

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Configuration
     */
    protected $config;


    public function __construct()
    {
        parent::__construct();
        $this->request = \Feazy\Common\DIManager::get('request');
        $this->config = \Feazy\Common\DIManager::get('config');
    }
}