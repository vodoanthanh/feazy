<?php
/**
 * Created by PhpStorm.
 * User: dinhthibc
 * Date: 10/5/18
 * Time: 4:48 PM
 */

namespace Application\Model;


use Feazy\Database\MySQL;

class Users extends MySQL
{
    public function all($orderBy, $sort, $start, $limit, $kw)
    {
        //get total
        $sth = $this->Slave->query("
			SELECT count(id)
			FROM users;\r\n
		");
        $sth->execute();

        $recordsTotal = $sth->fetchColumn();

        $searchKw= '';
        if (!empty(trim($kw))) {
            $searchKw = "
            WHERE id like '%$kw%'
			OR first_name like '%$kw%'  
			OR last_name like '%$kw%'  
			OR email like '%$kw%'  
			OR gender like '%$kw%'  
			OR ip_address like '%$kw%' ";
            //get recordsFiltered
            $sth = $this->Slave->query("
			SELECT count(id)
			FROM users 
			$searchKw
			;\r\n
		");
            $sth->execute();

            $recordsFiltered = $sth->fetchColumn();
        } else {
            $recordsFiltered = $recordsTotal;
        }

        $sth = $this->Slave->query("
			SELECT id, first_name, last_name, email, gender, ip_address  
			FROM users
			$searchKw
			ORDER BY $orderBy $sort
			LIMIT $start, $limit;\r\n
		");

        $sth->setFetchMode(\PDO::FETCH_ASSOC);

        $sth->execute();

        if ($recordsFiltered > 0) {
            $results = array();
            while ($row = $sth->fetch()) {
                $results[] = array(
                    $row['id'],
                    $row['first_name'],
                    $row['last_name'],
                    $row['email'],
                    $row['gender'],
                    $row['ip_address'],
                    "<div class='text-center'><a href='/users/{$row['id']}' class='btn btn-danger'>Edit</a></div>"
                );
            }

            return array($results, $recordsTotal, $recordsFiltered);
        }

        return array([], 0, 0);
    }

    public function get($id)
    {
        $sth = $this->Slave->prepare('
			SELECT id, first_name, last_name, email, gender, ip_address
			FROM users
			WHERE id = :id;\r\n
		');

        $sth->setFetchMode(\PDO::FETCH_ASSOC);

        $sth->execute(array(
            ':id' => $id
        ));

        while ($row = $sth->fetch()) {
            return $row;
        }

        return [];
    }

    public function insert($data)
    {
        $sth = $this->Slave->prepare("
			INSERT INTO users (first_name, last_name, email, gender, ip_address) 
			VALUES (
			'{$data['first_name']}',
			'{$data['last_name']}',
			'{$data['email']}',
			'{$data['gender']}',
			'{$data['ip_address']}');
			\r\n
		");

        $sth->execute();

        return $this->Slave->lastInsertId();
    }

    public function delete($id)
    {
        $sth = $this->Slave->prepare("
			DELETE FROM users WHERE id = $id;
			\r\n
		");

        return $sth->execute();
    }

    public function update($data)
    {
        $sth = $this->Slave->prepare("
			UPDATE users 
			SET first_name = '{$data['first_name']}', 
			last_name = '{$data['last_name']}',
			email = '{$data['email']}',
			gender = '{$data['gender']}',
			ip_address = '{$data['ip_address']}'
			WHERE id = {$data['id']};
			\r\n
		");

        $sth->execute();

        return $data['id'];
    }
}