<?php
/**
 * Created by PhpStorm.
 * User: dinhthibc
 * Date: 10/5/18
 * Time: 4:48 PM
 */

namespace Application\Model;


use Feazy\Database\MySQL;

class IndexModel extends MySQL
{
	public function get($id) {
		$sth = $this->Slave->prepare('
			SELECT * FROM users
			WHERE id = :id
		');
		$sth->execute(array(
			':id' => $id
		));

		while ($row = $sth->fetch()) {
			return $row;
		}

		return false;
	}
}