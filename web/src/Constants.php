<?php
/**
 * Created by PhpStorm.
 * User: thanhvd
 * Date: 2018-10-15
 * Time: 4:39 PM
 */

class Constants
{
    const GET = 'GET';
    const POST = 'POST';
    const DELETE = 'DELETE';
    const PUT = 'PUT';
}