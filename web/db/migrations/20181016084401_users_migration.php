<?php


use Phinx\Migration\AbstractMigration;

class UsersMigration extends AbstractMigration
{
    public function up()
    {
        $table = $this->table('users');
        $table
            ->addColumn('first_name', 'string', array('limit' => 50))
            ->addColumn('last_name', 'string', array('limit' => 50))
            ->addColumn('email', 'string', array('limit' => 50))
            ->addColumn('gender', 'string', array('limit' => 50))
            ->addColumn('ip_address', 'string', array('limit' => 20))
            ->save();
    }

    public function down()
    {
        $this->table('users')->drop()->save();
    }
}
