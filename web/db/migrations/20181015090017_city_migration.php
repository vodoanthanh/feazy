<?php


use Phinx\Migration\AbstractMigration;

class CityMigration extends AbstractMigration
{
    public function up()
    {
        $table = $this->table('city');
        $table
            ->addColumn('name', 'string', array('limit' => 20))
            ->addColumn('country', 'string', array('limit' => 20))
            ->addColumn('population', 'integer', array('limit' => 5000000))
            ->save();
    }

    public function down()
    {
        $this->table('city')->drop()->save();
    }
}
