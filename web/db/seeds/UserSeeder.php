<?php


use Phinx\Seed\AbstractSeed;

class UserSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = array(
            array(
                'first_name' => 'Lizzy',
                'last_name' => 'Sterry',
                'email' => 'lsterry0@apache.org',
                'gender' => 'Female',
                'ip_address' => '79.81.184.76',
            ),
            array(
                'first_name' => 'Winni',
                'last_name' => 'Sorey',
                'email' => 'wsorey9@craigslist.org',
                'gender' => 'Female',
                'ip_address' => '234.96.81.157',
            ),
            array(
                'first_name' => 'Anatole',
                'last_name' => 'Aldam',
                'email' => 'aaldamg@seesaa.net',
                'gender' => 'Male',
                'ip_address' => '54.209.219.69',
            ),
        );

        $users = $this->table('users');
        $users->insert($data)
            ->save();
    }
}
