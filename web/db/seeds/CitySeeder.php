<?php


use Phinx\Seed\AbstractSeed;

class CitySeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = array(
            array(
                'name' => 'Hồ Chí Minh',
                'country' => 'Việt Nam',
                'population' => 10000,
            ),
            array(
                'name' => 'Nha Trang',
                'country' => 'Việt Nam',
                'population' => 20000,
            ),
            array(
                'name' => 'Vũng Tàu',
                'country' => 'Việt Nam',
                'population' => 5201,
            ),
        );

        $cities = $this->table('city');
        $cities->insert($data)
            ->save();
    }
}
